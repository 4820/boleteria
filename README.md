***PRACTICO FINAL***

Este proyecto que he realizado, consta de un software programado bajo lenguaje Java, y utilizando como IDE Eclipse, que corre sobre mi maquina personal con SO Ubuntu. 

El software, esta desarrollado en base a una venta de  boletos para un teatro llamado Java. Donde se encarga primero que nada de guardar la información de la obra de teatro que se comercializará. Agregando asi, el nombre de la obra, los actores principales, y por ultimo el dia y la hora en que comenzará el espectáculo.

Luego de haber creado una nueva obra, se pasaria a la venta de boletos. Donde existen tres categorías: Standar (15 lugares), Premium (15 lugares), y Vip (10 lugares). Primero se pide colocarle un valor al precio Standar, y luego por defecto te devuelve un valor preestablecido de las butacas Premium con un 50% mas de incremento sobre el valor Standar. Y el Vip con un 100% de incremento sobre el precio Standar. 

Luego, se pasa a la venta de boletos, donde pide ingresar el tipo de boleto que desea adquirir el cliente, muestra la cantidad que quedan disponibles de esa categoria. Pide ingresar la cantidad de boletos que quiera comprar, y luego muestra el monto total que va a costar al adquirir la cierta cantidad de entradas que introdujo. 

Para observar luego la disponibilidad de butacas, sin la necesidad de comprar boletos, puede verlas mediante la opción "Disponibilidad", para ver que categorias de butacas quedan disponibles para ofrecer.